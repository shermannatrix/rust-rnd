fn main() {
	let a:[i32; 5] = [1, 2, 3, 4, 5];

	let b = [3; 5];	// [3, 3, 3, 3, 3]

	// accessing array elements
	let first = a[0];
	let second = a[1];
}
