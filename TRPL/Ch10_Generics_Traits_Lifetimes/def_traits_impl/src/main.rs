pub trait Summary {
	fn summarize_author(&self) -> String;

	fn summarize(&self) -> String {
		format!("(Read more from {}...)", self.summarize_author())
	}
}

pub struct NewsArticle {
	pub headline: String,
	pub location: String,
	pub author: String,
	pub content: String,
}

pub fn notify(item: impl Summary) {
	println!("Breaking news! {}", item.summarize());
}

impl Summary for NewsArticle {
	fn summarize_author(&self) -> String {
		format!("Written by {}", self.author)
	}
}

pub struct Tweet {
	pub username: String,
	pub content: String,
	pub reply: bool,
	pub retweet: bool,
}

impl Summary for Tweet {
	fn summarize_author(&self) -> String {
		format!("@{}", self.username)
	}
}

fn returns_summarizable(switch: bool) -> impl Summary {
	if switch {
		NewsArticle {
			headline: String::from("Penguins win the Stanley Cup Championship!"),
			location: String::from("Pittsburgh, PA, USA"),
			author: String::from("Iceburgh"),
			content: String::from("The Pittsburgh Penguins once again are the best hockey team in the NHL."),
		}
	} else {
		Tweet {
			username: String::from("horse_ebooks"),
			content: String::from("ofcourse, as your probably already know, people"),
			reply: false,
			retweet: false,
		}
	}
}

fn main() {
	// let article = NewsArticle {
	// 	headline: String::from("Penguins win the Stanley Cup Championship!"),
	// 	location: String::from("Pittsburgh, PA, USA"),
	// 	author: String::from("Iceburgh"),
	// 	content: String::from("The Pittsburgh Peguins once again are the best hockey team in the NHL."),
	// };

	// println!("New article available! {}", article.summarize());

	let tweet = Tweet {
		username: String::from("horse_ebooks"),
		content: String::from("ofcourse, as you probably already know, people"),
		reply: false,
		retweet: false,
	};

	println!("1 new tweet: {}", tweet.summarize());
}
